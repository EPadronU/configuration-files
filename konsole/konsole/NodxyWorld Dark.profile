[Appearance]
ColorScheme=nordfox
DimmValue=39
Font=FiraCode Nerd Font Mono,12,-1,5,57,0,0,0,0,0,Medium
TabColor=76,71,105
UseFontLineChararacters=true

[Cursor Options]
CustomCursorColor=255,0,0

[General]
DimWhenInactive=true
InvertSelectionColors=true
LocalTabTitleFormat=%w
Name=NodxyWorld Dark
Parent=FALLBACK/
RemoteTabTitleFormat=[%u@%H]  %w
TerminalColumns=120
TerminalMargin=2
TerminalRows=26

[Interaction Options]
AutoCopySelectedText=true
TextEditorCmd=1
TrimLeadingSpacesInSelectedText=true
TrimTrailingSpacesInSelectedText=true
UnderlineFilesEnabled=true
WordCharacters=-_

[Scrolling]
HistorySize=5000
ScrollBarPosition=2

[Terminal Features]
BellMode=0
BlinkingCursorEnabled=true
ReverseUrlHints=true
